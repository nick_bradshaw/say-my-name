# Say My Name README #

Say My Name is an app meant to make you and your friends laugh by generating your very own unique stripper name! First off let me say that this app is meant to be a fun app to make people smile. It's not intended to be offensive, obscene or anything else other than fun. It's also not meant to cheapen the chosen profession of exotic dancing. Just want to get that out of the way.

To get started- 
Open up Android Studio and simply select "Import Project"

Once you have the app imported choose Run 'app' from the Run dropdown menu.

From there- follow the on-screen directions. 
At the start of the app you can choose whether or not your starting and ending images are male or female.

From their you'll be taken to a screen where the app will ask you to input some information needed to come up with your stripper name. 

Once you're finished with that you can choose from one of three types of stripper names using three separate buttons.

When you get to the final activity your name will be revealed. You can hit the share button to share it with friends and there is a popup menu with a few more options.

Enjoy!